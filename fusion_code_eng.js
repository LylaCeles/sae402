function fusion() {
    let paint;
    let clickColor = new Array()
    let colorBlue = "#0000ff";
    let colorViolet = "#ff00ff";
    let colorBlack = "#000000";
    let couleurActive = "#000000";
    let bleu = false;
    let violet = false;


    document.getElementById("stopButton").addEventListener("click", function () {
        //bouton stop
        paint = false;
        positionLoop();


    })
    document.getElementById("startButton").addEventListener("click", function () {
        //bouton start
        paint = true;
        positionLoop();


    })


    document.getElementById("clearButton").addEventListener("click", function () {

        clickX = [];
        clickY = [];
        clickDrag = [];

        cc.clearRect(0, 0, cc.canvas.width, cc.canvas.height);

        // window.location.replace("./index.html");
    });




    function couleur(event) {
        paint = false;
        positionLoop();
        switch (event.key) {
            case 'b':
                bleu = !bleu;
                if (bleu) {

                    if (violet) {
                        violet = !violet;
                    }
                    couleurActive = colorBlue;
                    paint = true;
                }
                else {
                    paint = false;
                }
                break;
            case 'c':
                violet = !violet;
                if (violet) {
                    if (bleu) {
                        bleu = !bleu;
                    }
                    couleurActive = colorViolet;
                    paint = true;
                }
                else {
                    paint = false;

                }

                break;
        }
        positionLoop();
        // return couleurActive;
    }
    document.addEventListener('keydown', couleur);



    // ******************************************//


    let video = document.querySelector('#inputVideo');
    if (!video) {
        console.log("Erreur: la balise video n'existe pas");
        return;
    }

    navigator.mediaDevices.getUserMedia({ video: true })
        .then(function (stream) {
            let video = document.querySelector('#inputVideo');
            video.srcObject = stream;
            video.play();
        })
        .catch(function (err) {
            console.log("Erreur: " + err);
        });

    let videoInput = document.getElementById('inputVideo');

    let ctracker = new clm.tracker();
    ctracker.init();
    ctracker.start(videoInput);

    let clickX = new Array;
    let clickY = new Array;
    let clickDrag = new Array;

    function positionLoop() {
        // console.log("couocu")
        if (paint) {
            // console.log("positionloop")
            clickColor.push(couleurActive);


            requestAnimationFrame(positionLoop);
            let positions = ctracker.getCurrentPosition();
            let positionString = "";
            if (positions) {
                for (let p = 0; p < 70; p++) {
                    positions[p][0].pop;
                    positions[p][1].pop;
                    if (p == 62) {
                        clickX.push(positions[p][0].toFixed(2));
                        clickY.push(positions[p][1].toFixed(2));
                    }
                }

            }
            document.getElementById('positions').innerHTML = positionString;
        }
    }

    positionLoop();

    let canvasInput = document.getElementById('drawCanvas');
    let cc = canvasInput.getContext('2d');
    function drawLoop() {
        requestAnimationFrame(drawLoop);
        cc.clearRect(0, 0, canvasInput.width, canvasInput.height);
        // ctracker.draw(canvasInput);

        // cc.strokeStyle = "#df4b26";
        cc.lineJoin = "round";
        cc.lineWidth = 5;


        for (let i = 0; i < clickX.length; i++) {
            cc.beginPath();
            if (clickDrag[i] && i) {
                cc.moveTo(clickX[i - 1], clickY[i - 1]);
            } else {
                cc.moveTo(clickX[i] - 1, clickY[i]);
            }
            cc.lineTo(clickX[i], clickY[i]);
            cc.strokeStyle = clickColor[i];
            cc.closePath();
            cc.stroke();
        }


    }
    drawLoop();




    let tabloMot = ["Flowers",
    "Stars",
    "Sun",
    "Clouds",
    "Trees",
    "Leaves",
    "Fruit",
    "Vegetables",
    "Smiling Man",
    "Sad man",
    "Heart",
    "Lightning",
    "Moon",
    "Mountain",
    "Ocean",
    "House",
    "Kitten",
    "Puppy",
    "Rabbit",
    "Bird",
    "Goldfish",
    "Snail",
    "Turtle",
    "Crab",
    "Spider",
    "Pumpkin",
    "Scarecrow",
    "Witch",
    "Ghost",
    "Bat",
    "Lemon",
    "Apple",
    "Banana",
    "Orange",
    "Carrot",
    "Courgette",
    "Tomato",
    "Cucumber",
    "Mushroom",
    "Broccoli",
    "Chicken",
    "Pizza",
    "Hamburger",
    "Hot dog",
    "Chips",
    "Ice cream",
    "Cake",
    "Candy",
    "Chocolate",
    "Biscuit",
    "Bubbles",
    "Fireplace",
    "Car",
    "Airplane",
    "Boat",
    "Train",
    "Bus",
    "Bicycle",
    "Tennis racket",
    "Football",
    "Basketball",
    "Ball"]

    document.getElementById("motButton").addEventListener("click", function () {
        let mot = Math.floor(Math.random() * (tabloMot.length - 0 + 1)) + 0

        let divR = document.getElementById("div Random");
        divR.textContent = tabloMot[mot];

    });





}

window.addEventListener("load", fusion);
