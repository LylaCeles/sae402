function Canva() {
	let canvasDiv = document.getElementById('canvasDiv');
	let context = document.getElementById('canvasInAPerfectWorld').getContext("2d");
	console.log(context)
	canvas = document.createElement('canvas');
	canvas.setAttribute('width', 490);
	canvas.setAttribute('height', 220);
	canvas.setAttribute('id', 'canvas');
	canvasDiv.appendChild(canvas);
	if (typeof G_vmlCanvasManager != 'undefined') {
		canvas = G_vmlCanvasManager.initElement(canvas);
	}
	context = canvas.getContext("2d");



	let clickX = new Array();
	let clickY = new Array();
	let clickDrag = new Array();
	let paint;
	canvas.addEventListener("mousedown", function(e) {
		let mouseX = e.pageX;
		let mouseY = e.pageY;
		console.log(mouseX)
		console.log(mouseY)

		paint = true;
		addClick(mouseX, mouseY);
		redraw();
	});


	canvas.addEventListener("mousemove", function (e) {
		if (paint) {
			// console.log(this.offsetLeft);
			addClick(e.pageX, e.pageY, true);
			redraw();
		}
	})

	canvas.addEventListener("mouseup", function (e) {
		paint = false;

	});
	let colorBlue = "#0000ff";
	let colorViolet = "#ff00ff";
	let colorBlack = "#000000";
	let couleurActive = "#000000";
	let bleu = false;
	let violet = false;
	function couleur(event) {
		switch (event.key) {
			case 'b':
				bleu = !bleu;
				if (bleu) {
					console.log('dessine en bleu');
					couleurActive = colorBlue;
				}
				else {
					console.log('Nuar');
					couleurActive = colorBlack;
				}
				break;
				case 'c':
					violet = !violet;
					if (violet) {
						console.log('dessine en Violet');
						couleurActive = colorViolet;
					}	
					else {
						console.log('Nuar');
						couleurActive = colorBlack;
					}
				
				break;
		}
		// return couleurActive;
	}



	function redraw() {

		context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
		// context.strokeStyle = couleurActive;
		context.lineJoin = "round";
		context.lineWidth = 5;
		clickColor.push(couleurActive);


		for (let i = 0; i < clickX.length; i++) {
			context.beginPath();
			if (clickDrag[i] && i) {
				context.moveTo(clickX[i - 1], clickY[i - 1]);
			} else {
				context.moveTo(clickX[i] - 1, clickY[i]);
			}
			context.lineTo(clickX[i], clickY[i]);
			context.closePath();
			context.strokeStyle = clickColor[i];
			context.stroke();
		}
	}



	document.addEventListener('keydown', couleur);

	let clickColor = new Array()

	function addClick(x, y, dragging) {
		clickX.push(x);
		clickY.push(y);
		clickDrag.push(dragging);
		// clickColor.push(couleurActive);
	}


	document.getElementById("clearButton").addEventListener("click", function () {

		clickX = [];
		clickY = [];
		clickDrag = [];

		context.clearRect(0, 0, context.canvas.width, context.canvas.height);
		// window.location.replace("./index.html");
	});


}


window.addEventListener("load", Canva);